# Table of Content

## Day 1
- Module 1:  Introduction to Docker (90 mins)
- Module 2:  Working with Containers (90 mins)
- Module 3:  Docker Images and its storage structure (90 mins)
- Module 4:  Building your own image, and deep drive into Dockerfile (90 mins)

## Day 2
- Module 5: Docker Networking (Single Host) (90 mins)
- Module 6:  Orchestration using Docker Compose (90 mins)
- Module 7: The DevOps play using Docker (90 mins)
- Module 8: Container Anti-patterns and best practices (45 mins)
- Module 9:  Troubleshooting (45 mins)

## Day 3
- Module 10: Introduction to Docker Swarm – Docker native Clustering (90 mis)
- Module 11: Multi-host networking (90 mins)
- Module 12: Advance Docker Service operations (90 mins)
- Module 13: Orchestration using Docker stack (45 mins)
- Module 14: Docker node management (45 mins)